interface Rectangle {
    width: number;
    height: number;
}
interface ColoreRectangle extends Rectangle {
    color: string
}
const rectangle: Rectangle = {
    width: 20,
    height: 10
}
console.log(rectangle);

const coloreRectangle: ColoreRectangle = {
    width: 20,
    height: 10,
    color: "red"
}
console.log(coloreRectangle);