class Person {
    public constructor(private readonly name: string) {
      }
      public getName(): string {
        return this.name;
      }
}

const person = new Person("Jane");
console.log(person);
console.log(person.getName());

interface Shape {
  getArea: () => number;
}

class Rectangle implements Shape {
  public constructor(protected readonly width1: number, protected readonly height1: number) {}

  public getArea(): number {
    return this.width1 * this.height1;
  }
}

const rect: Rectangle =  new Rectangle(50,10);
console.log(rect);
console.log(rect.getArea());

class Square extends Rectangle {
  public constructor(width1: number) {
    super(width1,width1);
  }
  public toString(): string {
    return `Square[${this.width1}]`;
  }
}

const sq = new Square(10);
console.log(sq);
console.log(sq.getArea());
console.log(sq.toString());

abstract class Polygon {
  public abstract getArea(): number; 
  public toString(): string {
    return `Polygon[area=${this.getArea()}]`;
  }
}
class Rectangle2 extends Polygon {
  public constructor(protected readonly width: number, protected readonly height: number) {
    super();
  }

  public getArea(): number {
    return this.width * this.height;
  }
}

const rect2: Rectangle2 =  new Rectangle2(50,10);
console.log(rect2);
console.log(rect2.getArea());