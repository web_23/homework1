let k: unknown = "hello";
let num: string = "1";
console.log((k as string).length);
console.log((<string>k).length);
console.log(((num as unknown)as number).toFixed(2));